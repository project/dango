<?php
/**
* Return an array of the modules to be enabled when this profile is installed.
*
* @return
*  An array of modules to be enabled.
*/
function dango_profile_modules() {
  
  // Required core modules
  $required = array('block', 'filter', 'node', 'system', 'user');
    
    //Optional core modules
  $optional = array('help', 'menu', 'taxonomy', 'contact', 'path');

    //Contributed modules 
    $contrib = array('access_perm_group', 'activity_dashboard', 'bounced_email', 'content', 'text', 'fieldgroup', 'deliver', 'features', 'filefield', 'geocode', 'geocode_bing', 'html2txt', 'identity_hash', 'imageapi', 'imagecache', 'imagecache_ui', 'imagefield', 'imageapi_gd', 'pgapi', 'publication', 'schedule', 'subscribed', 'subscribed_silverlight', 'templates', 'token', 'enewsletter', 'windowslivemessenger', 'pg_fundraising', 'fundraising_feature');
    return array_merge($required, $optional, $contrib);

}

/**
* Return a description of the profile for the initial installation screen.
*
* @return
*   An array with keys 'name' and 'description' describing this profile.
*/
function dango_profile_details() {
  return array(
    'name' => 'DANGO (Drupal and NGOs)',
    'description' => 'This install profile will install a number of useful tools for NGOs and charities.',
  );
}

/**
 * Return a list of tasks that this profile supports.
 *
 * @return
 *   A keyed array of tasks the profile will perform during
 *   the final stage. The keys of the array will be used internally,
 *   while the values will be displayed to the user in the installer
 *   task list.
 */
function dango_profile_task_list() {
}

function dango_profile_tasks(&$task, $url) {
  
  //create content types
  dango_add_content_types();
  
  //add nodes
  //TODO: add a documentation page
  //dango_add_nodes();
  
  //set variables
  dango_set_variables();
  
  // Update the menu router information.
  menu_rebuild();


}


function dango_add_content_types(){
  // Insert default user-defined node types into the database. For a complete
  // list of available node type attributes, refer to the node type API
  // documentation at: http://api.drupal.org/api/HEAD/function/hook_node_info.
  $types = array(
    array(
      'type' => 'page',
      'name' => st('Page'),
      'module' => 'node',
      'description' => st("A <em>page</em>, similar in form to a <em>story</em>, is a simple method for creating and displaying information that rarely changes, such as an \"About us\" section of a website. By default, a <em>page</em> entry does not allow visitor comments and is not featured on the site's initial home page."),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
      'help' => '',
      'min_word_count' => '',
    ),
    array(
      'type' => 'story',
      'name' => st('Story'),
      'module' => 'node',
      'description' => st("A <em>story</em>, similar in form to a <em>page</em>, is ideal for creating and displaying content that informs or engages website visitors. Press releases, site announcements, and informal blog-like entries may all be created with a <em>story</em> entry. By default, a <em>story</em> entry is automatically featured on the site's initial home page, and provides the ability to post comments."),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
      'help' => '',
      'min_word_count' => '',
    ),
  );

  foreach ($types as $type) {
    $type = (object) _node_type_set_defaults($type);
    node_type_save($type);
  }
  
}


function dango_set_variables(){

  //use content type pg_fundraising to create donation pages
  variable_set('pg_fundraising_allowed_pg_fundraising', '1');

  // Default page to not be promoted and have comments disabled.
  variable_set('node_options_page', array('status'));
  variable_set('comment_page', COMMENT_NODE_DISABLED);

  // Don't display date and author information for page nodes by default.
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['toggle_node_info_page'] = FALSE;
  variable_set('theme_settings', $theme_settings);  
}

function dango_add_nodes(){
	
	module_load_include('inc', 'node', 'node.pages');
  
  //'Welcome' page
  $node = array('type' => 'page') ;
  $form_state = array();
  $form_state['values']['uid'] = 1;
  $form_state['values']['title'] = 'Welcome to DANGO!';
  $form_state['values']['type'] = 'page';
  $form_state['values']['body'] = '<p>DaNGO is an install profile designed to empower small NGOs. It comes with the following modules:</p>
<ul>
<li>Windows Live Messenger: http://drupal.org/project/windowslivemessenger</li>
<li>Geocode Bing and its dependent modules: http://drupal.org/project/geocode_bing</li>
<li>eNewsletter and its dependent modules: http://drupal.org/project/enewsletter</li>
<li>Activity Dashboard: http://drupal.org/project/activity_dashboard</li>
<li>Fundraising and its dependent modules: http://drupal.org/project/pg_fundraising</li>
</ul>
<p>To set up these modules, please visit the project page for more information.</p>';
  
  $form_state['values']['path'] = 'welcome';
  $form_state['values']['comment'] = '0';
  $form_state['values']['menu'] = array('link_title'=>'Welcome', 'description'=>'About DANGO', 'pmid'=>'2', 'weight'=>'-10', 'menu_name' => 'primary-links');
  $form_state['values']['status'] = '1';
  $form_state['values']['format'] = '2';
  $form_state['values']['op'] = t('Save');

  drupal_execute('page_node_form', $form_state, (object)$node);  
    

  //'About us' page
  $node = array('type' => 'page') ;
  $form_state = array();
  $form_state['values']['uid'] = 1;
  $form_state['values']['title'] = 'About us';
  $form_state['values']['type'] = 'page';
  $form_state['values']['body'] = '<p>This is a stub.</p>';
  $form_state['values']['path'] = 'about';
  $form_state['values']['comment'] = '0';
  $form_state['values']['menu'] = array('link_title'=>'About us', 'description'=>'About us', 'pmid'=>'2', 'weight'=>'-8', 'menu_name' => 'primary-links');
  $form_state['values']['status'] = '1';
  $form_state['values']['format'] = '2';
  $form_state['values']['op'] = t('Save');

  drupal_execute('page_node_form', $form_state, (object)$node);  

  
}
